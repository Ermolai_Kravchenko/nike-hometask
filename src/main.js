import "./style.css";

import { createContainer } from "./components/container/container.js";
import { createHeader } from "./components/header/header.js";
import { createMainContent } from "./components/main-content/main-content.js";
import { createFooter } from "./components/footer/footer";

const app = document.getElementById("app");

const container = createContainer();
app.appendChild(container);

const header = createHeader();
container.appendChild(header);

const main = createMainContent();
container.appendChild(main);

const footer = createFooter();
container.appendChild(footer);
