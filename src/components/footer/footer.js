import "./footer.css";
import whiteStarImg from "../../assets/pictures/white-star.svg";
import noneWhiteStarImg from "../../assets/pictures/none-white-star.svg";

function createFooter() {
  const footer = document.createElement("footer");
  footer.classList.add("footer");

  const ul = document.createElement("ul");
  ul.classList.add("footer__ul");
  const li1 = document.createElement("li");
  li1.classList.add("footer__li1");
  li1.textContent = "749";
  const li2 = document.createElement("li");
  li2.classList.add("footer__li2");
  const li3 = document.createElement("li");
  li3.classList.add("footer__li3");
  li3.textContent =
    "A Woocommerce product gallery Slider for Slider Revolution with mind-blowing visuals.";
  footer.appendChild(ul);
  ul.appendChild(li1);
  ul.appendChild(li2);
  ul.appendChild(li3);

  const starUl = document.createElement("ul");
  starUl.classList.add("footer__starUl");
  const starLi1 = document.createElement("li");
  starLi1.classList.add("footer__starLi1");
  const starLi2 = document.createElement("li");
  starLi2.classList.add("footer__starLi2");
  const starLi3 = document.createElement("li");
  starLi3.classList.add("footer__starLi3");
  const starLi4 = document.createElement("li");
  starLi4.classList.add("footer__starLi4");
  const starLi5 = document.createElement("li");
  starLi5.classList.add("footer__starLi5");
  li2.appendChild(starUl);
  starUl.appendChild(starLi1);
  starUl.appendChild(starLi2);
  starUl.appendChild(starLi3);
  starUl.appendChild(starLi4);
  starUl.appendChild(starLi5);

  const starImg1 = document.createElement("img");
  starImg1.src = whiteStarImg;
  starImg1.alt = "Star Img";
  starImg1.classList.add("footer__star-img");
  starLi1.appendChild(starImg1);

  const starImg2 = document.createElement("img");
  starImg2.src = whiteStarImg;
  starImg2.alt = "Star Img";
  starImg2.classList.add("footer__star-img");
  starLi2.appendChild(starImg2);

  const starImg3 = document.createElement("img");
  starImg3.src = whiteStarImg;
  starImg3.alt = "Star Img";
  starImg3.classList.add("footer__star-img");
  starLi3.appendChild(starImg3);

  const starImg4 = document.createElement("img");
  starImg4.src = whiteStarImg;
  starImg4.alt = "Star Img";
  starImg4.classList.add("footer__star-img");
  starLi4.appendChild(starImg4);

  const starImg5 = document.createElement("img");
  starImg5.src = noneWhiteStarImg;
  starImg5.alt = "Star Img";
  starImg5.classList.add("footer__star-img");
  starLi5.appendChild(starImg5);

  const button = document.createElement("button");
  button.classList.add("footer__button");
  button.textContent = "BUY NOW";
  footer.appendChild(button);

  return footer;
}

export { createFooter };
