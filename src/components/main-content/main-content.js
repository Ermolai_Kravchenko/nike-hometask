import "./main-content.css";
import mainIMG from "../../assets/pictures/air-jordan.svg";
import shadowImgUrl from "../../assets/pictures/shadow.svg";
import buttonImgUrl from "../../assets/pictures/Ellipse.svg";
import vectorImgUrl from "../../assets/pictures/Vector.svg";

function createMainContent() {
  const main = document.createElement("main");
  main.classList.add("main");

  const mainSection = document.createElement("section");
  mainSection.classList.add("main-section");
  main.appendChild(mainSection);

  const mainPhoto = document.createElement("div");
  mainPhoto.classList.add("main-section__main-photo");
  mainSection.appendChild(mainPhoto);

  const mainImg = document.createElement("img");
  mainImg.setAttribute("src", mainIMG);
  mainImg.setAttribute("alt", "Main Photo");
  mainImg.classList.add("main-section__img-main-photo");
  mainPhoto.appendChild(mainImg);

  const mainTitle = document.createElement("h1");
  mainTitle.classList.add("main-section__main-title");
  mainTitle.textContent = "NIKE AIR JORDAN";
  mainPhoto.appendChild(mainTitle);

  const mainTitle2 = document.createElement("h1");
  mainTitle2.classList.add("main-section__main-title-2");
  mainTitle2.textContent = "NIKE AIR JORDAN";
  mainPhoto.appendChild(mainTitle2);

  const shadowImg = document.createElement("img");
  shadowImg.setAttribute("src", shadowImgUrl);
  shadowImg.setAttribute("alt", "Shadow Photo");
  shadowImg.classList.add("main-section__shadow-photo");
  mainPhoto.appendChild(shadowImg);

  const button = document.createElement("button");
  button.classList.add("main-section__button");
  mainSection.appendChild(button);

  const buttonSection = document.createElement("section");
  buttonSection.classList.add("main-section__button-section");
  button.appendChild(buttonSection);

  const buttonImg1 = document.createElement("img");
  buttonImg1.setAttribute("src", buttonImgUrl);
  buttonImg1.setAttribute("alt", "Button Photo");
  buttonImg1.classList.add("main-section__button-photo-ellipse");
  buttonSection.appendChild(buttonImg1);

  const buttonImg2 = document.createElement("img");
  buttonImg2.setAttribute("src", vectorImgUrl);
  buttonImg2.setAttribute("alt", "Button Photo");
  buttonImg2.classList.add("main-section__button-photo-vector");
  buttonSection.appendChild(buttonImg2);

  const leftButton = document.createElement("button");
  leftButton.classList.add("main-section__leftButton");
  mainSection.appendChild(leftButton);

  const leftButtonSection = document.createElement("section");
  leftButtonSection.classList.add("main-section__leftButton-section");
  leftButton.appendChild(leftButtonSection);

  const leftButtonImg1 = document.createElement("img");
  leftButtonImg1.setAttribute("src", buttonImgUrl);
  leftButtonImg1.setAttribute("alt", "Button Photo");
  leftButtonImg1.classList.add("main-section__leftButton-photo-ellipse");
  leftButtonSection.appendChild(leftButtonImg1);

  const leftButtonImg2 = document.createElement("img");
  leftButtonImg2.setAttribute("src", vectorImgUrl);
  leftButtonImg2.setAttribute("alt", "Button Photo");
  leftButtonImg2.classList.add("main-section__leftButton-photo-vector");
  leftButtonSection.appendChild(leftButtonImg2);

  return main;
}

export { createMainContent };
