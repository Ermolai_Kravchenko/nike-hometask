import "./container.css";

function createContainer() {
  const container = document.createElement("div");
  container.classList.add("container");
  return container;
}

export { createContainer };
