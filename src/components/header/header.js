import "./header.css";
import nikeLogoImg from "../../assets/logos/nikelogo.svg";
import twitterLogoImg from "../../assets/logos/twitter-logo.svg";
import instaLogoImg from "../../assets/logos/insta-logo.svg";
import facebookLogoImg from "../../assets/logos/facebook-logo.svg";

function createHeader() {
  const header = document.createElement("header");
  header.classList.add("header");

  const logoDiv = document.createElement("div");
  logoDiv.classList.add("header__logo");
  header.appendChild(logoDiv);

  const nikeImg = document.createElement("img");
  nikeImg.src = nikeLogoImg;
  nikeImg.alt = "Nike Logo";
  nikeImg.classList.add("header__logo-nike-img");
  logoDiv.appendChild(nikeImg);

  const textDiv = document.createElement("div");
  textDiv.classList.add("header__text-div");
  header.appendChild(textDiv);

  const headerslider = document.createElement("span");
  headerslider.classList.add("header__slider");
  headerslider.textContent = "WOOCOMMERCE  PRODUCT  SLIDER";
  textDiv.appendChild(headerslider);

  const nav = document.createElement("nav");
  nav.classList.add("header__nav");
  header.appendChild(nav);

  const twitterImg = document.createElement("img");
  twitterImg.src = twitterLogoImg;
  twitterImg.alt = "Twitter Logo";
  twitterImg.classList.add("header__logo-twitter-img");
  nav.appendChild(twitterImg);

  const instaImg = document.createElement("img");
  instaImg.src = instaLogoImg;
  instaImg.alt = "Instagram Logo";
  instaImg.classList.add("header__logo-instagram-img");
  nav.appendChild(instaImg);

  const facebookImg = document.createElement("img");
  facebookImg.src = facebookLogoImg;
  facebookImg.alt = "Facebook Logo";
  facebookImg.classList.add("header__logo-facebook-img");
  nav.appendChild(facebookImg);

  const cartDiv = document.createElement("div");
  cartDiv.classList.add("header__cart");

  const cartText = document.createElement("span");
  cartText.classList.add("header__cart-text");
  cartText.textContent = "CART (0)";
  cartDiv.appendChild(cartText);
  header.appendChild(cartDiv);

  return header;
}

export { createHeader };
